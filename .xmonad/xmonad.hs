import XMonad
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.NoBorders
import XMonad.Layout.ToggleLayouts
import XMonad.Util.Run

import qualified XMonad.StackSet as W
import qualified Data.Map as M

import System.Exit

myTerminal = "alacritty"

myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    [ ((modMask .|. controlMask, xK_p), spawn "sudo acpiconf -s 3")

    , ((modMask .|. shiftMask, xK_e), io (exitWith ExitSuccess))
    , ((modMask .|. shiftMask, xK_r), restart "xmonad" True)
    , ((modMask .|. shiftMask, xK_q), kill)
    , ((modMask .|. shiftMask, xK_space), setLayout $ XMonad.layoutHook conf)

    , ((modMask , xK_Return), spawn $ XMonad.terminal conf)
    , ((modMask , xK_space), sendMessage NextLayout)
    , ((modMask , xK_t), withFocused $ windows . W.sink)
    , ((modMask , xK_Tab), windows W.focusDown)

    , ((modMask , xK_d), spawn "rofi -show run")
    , ((modMask , xK_s), spawn "rofi -show ssh")
    , ((modMask , xK_w), spawn "rofi -show window")
    ]
    ++
    [ ((m .|. modMask, k), windows $ f i)
    | (i, k) <- zip (XMonad.workspaces conf) [xK_F1 .. xK_F9]
    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]
    ]

main = do
    h <- spawnPipe "xmobar ~/.xmobar/xmobar.hs"

    xmonad $ ewmh defaultConfig
        { terminal = myTerminal
        , layoutHook = avoidStruts $ toggleLayouts(noBorders Full) $ smartBorders $ layoutHook defaultConfig
        , logHook = dynamicLogWithPP $  xmobarPP
            { ppTitle            = shorten 100
            , ppCurrent          = xmobarColor "#A0A0A0" "" . wrap "[" "]"
            , ppHidden           = xmobarColor "#404040" ""
            , ppHiddenNoWindows  = xmobarColor "#404040" ""
            , ppOutput           = hPutStrLn h
            }
        , manageHook = manageDocks <+> manageHook defaultConfig
        , handleEventHook = handleEventHook defaultConfig <+> docksEventHook
        , keys     = myKeys
        }
