Config { font = "xft:Fixed-8"
       , bgColor = "black"
       , fgColor = "grey"
       , position = Top
       , lowerOnStart = False
       , commands =
            [ Run Weather "EBCI" ["-t","<station>: <tempC>C","-L","18","-H","25","--normal","green","--high","red","--low","lightblue"] 36000
   		    , Run Date "%a %b %_d %Y %H:%M:%S" "date" 10
            , Run Com "/bin/sh" ["-c", "ifconfig wlan0 | grep 'inet' | awk -F ' ' '{ print $2 }'"] "wlan0" 5
            , Run Com "/bin/sh" ["-c", "vmstat -h | tail -n 1 | tr -s ' ' | cut -d' ' -f5"] "freemem" 1
            , Run Com "/bin/sh" ["-c", " vmstat -h | tail -n 1 | tr -s ' ' | cut -d ' ' -f17 | sed 's/$/%/g'"] "cpuus" 1
            , Run Com "/bin/sh" ["-c", " vmstat -h | tail -n 1 | tr -s ' ' | cut -d ' ' -f18 | sed 's/$/%/g'" ] "cpusy" 1
            , Run StdinReader
            ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "%StdinReader% }{ wlan0: %wlan0% cpu us: %cpuus% cpu sys: %cpusy% freemem: %freemem% <fc=#ee9a00>%date%</fc> | %EBCI% | %uname%" 
       }
